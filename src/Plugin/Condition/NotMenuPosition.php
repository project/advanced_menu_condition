<?php

namespace Drupal\advanced_menu_condition\Plugin\Condition;

/**
 * Hide block on selected menu items.
 *
 * @Condition(
 *   id = "advanced_menu_position_not",
 *   label = @Translation("Advanced Menu position - Hide"),
 * )
 */
class NotMenuPosition extends MenuPosition {

  /**
   * {@inheritdoc}
   */
  protected $configKey = 'not_menu_position';

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    if (empty($this->configuration[$this->configKey]) || empty(reset($this->configuration[$this->configKey]))) {
      return TRUE;
    }
    return !parent::evaluate();
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {
    return $this->t(
      'The selected menu items are neither active nor in the active trail.'
    );
  }

}
