<?php

namespace Drupal\advanced_menu_condition\Plugin\Condition;

use Drupal\Core\Condition\ConditionInterface;
use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Menu\MenuActiveTrailInterface;
use Drupal\Core\Menu\MenuLinkManagerInterface;
use Drupal\Core\Menu\MenuParentFormSelectorInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Show block on selected menu items.
 *
 * @Condition(
 *   id = "advanced_menu_position",
 *   label = @Translation("Advanced menu position"),
 * )
 */
class MenuPosition extends ConditionPluginBase implements ConditionInterface, ContainerFactoryPluginInterface {
  use StringTranslationTrait;

  /**
   * The menu active trail service.
   *
   * @var \Drupal\Core\Menu\MenuActiveTrailInterface
   */
  protected $menuActiveTrail;

  /**
   * The menu parent form selector service.
   *
   * @var \Drupal\Core\Menu\MenuParentFormSelectorInterface
   */
  protected $menuParentFormSelector;

  /**
   * The plugin manager menu link service.
   *
   * @var \Drupal\Core\Menu\MenuLinkManagerInterface
   */
  protected $pluginManagerMenuLink;

  /**
   * The configuration key.
   *
   * @var string
   */
  protected $configKey = 'menu_position';

  /**
   * Creates a MenuPosition instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Menu\MenuActiveTrailInterface $menu_active_trail
   *   The menu active trail service.
   * @param \Drupal\Core\Menu\MenuParentFormSelectorInterface $menu_parent_form_selector
   *   The menu parent form selector service.
   * @param \Drupal\Core\Menu\MenuLinkManagerInterface $plugin_manager_menu_link
   *   The plugin manager menu link service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MenuActiveTrailInterface $menu_active_trail, MenuParentFormSelectorInterface $menu_parent_form_selector, MenuLinkManagerInterface $plugin_manager_menu_link) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->menuActiveTrail = $menu_active_trail;
    $this->menuParentFormSelector = $menu_parent_form_selector;
    $this->pluginManagerMenuLink = $plugin_manager_menu_link;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('menu.active_trail'),
      $container->get('menu.parent_form_selector'),
      $container->get('plugin.manager.menu.link')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    $selectedTrails = [];
    $menus = [];
    $active_trail_ids = [];
    if (empty($this->configuration[$this->configKey]) || empty(reset($this->configuration[$this->configKey]))) {
      return TRUE;
    }

    foreach ($this->configuration[$this->configKey] as $trailId) {
      $parts = explode(":", (string) $trailId);
      $menus[] = $parts[0];
      $selectedTrails[] = $trailId;
    }
    foreach ($menus as $menu) {
      $active_trail_ids[$menu] = $this->menuActiveTrail->getActiveTrailIds($menu);
    }
    foreach ($selectedTrails as $selectedTrail) {
      $parts = explode(":", (string) $selectedTrail);
      if (count($parts) >= 3 && in_array($parts[1] . ':' . $parts[2], $active_trail_ids[$parts[0]])) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {
    return $this->t(
      'The selected menu items are either active or in the active trail.'
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form[$this->configKey] = [
      '#type' => 'select',
      '#multiple' => TRUE,
      '#title' => $this->t("Menu parent"),
      '#description' => $this->t("Show the block on this menu item and all its children only."),
    ];
    $form[$this->configKey]['#options'] = ['' => $this->t("- None -")] + $this->menuParentFormSelector->getParentSelectOptions();
    $form[$this->configKey]['#default_value'] = $this->configuration[$this->configKey];
    $form[$this->configKey]['#attributes'] = ['style' => 'height:400px;width:100%;'];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration[$this->configKey] = $form_state->getValue($this->configKey);
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [$this->configKey => ''] + parent::defaultConfiguration();
  }

}
